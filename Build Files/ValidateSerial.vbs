Option Explicit

Dim xSer
Dim xSep
Dim xFullLic
Dim xCode
Dim xCodeMod
Dim i
Dim xDate
Dim x
Dim xPart1
Dim xPart2
Dim xPart3
Dim xSerial
Dim d

Dim xFeat
Dim xComponents

Function ValidateSerial()
    Dim bIsValidKey
	Dim bIsValidFeature

    xFullLic = Session.Property("FULLLICENSEKEY")
    xFullLic = Trim(xFullLic)
	'MsgBox	xFullLic
'get separator
    xSep = InStr(xFullLic, ":::")
    
    If (xSep <> 0) Then
		xSep = xSep + 1
	Else
		xSep = InStr(xFullLic, "::")
    End If
    
'validate entry
    If Len(xFullLic) < 28 Or (xSep = 0) Then
        Session.Property("LicenseKeyIsValid") = "0"
        ValidateSerial = 0
    Else
   '   get key part & validate
        xSer = Mid(xFullLic, Int(xSep) + 2)
        bIsValidKey = ValidDateKey(xSer)
		
        On Error resume next
        xComponents = DecryptComponent(Left(xFullLic, Int(xSep) - 1))
				
        bIsValidFeature = True
        
        select case CInt(xComponents)
			case 1
				Session.Property("FEATURE_MACPAC") = "1"
				Session.Property("INSTALL_MACPAC") = "1"
				Session.Property("FEATURE_NUMBERING") = ""
				Session.Property("INSTALL_NUMBERING") = ""
				Session.Property("FEATURE_TRAILER") = ""
				Session.Property("INSTALL_TRAILER") = ""
			case 2
				Session.Property("FEATURE_MACPAC") = ""
				Session.Property("INSTALL_MACPAC") = ""
				Session.Property("FEATURE_NUMBERING") = "1"
				Session.Property("INSTALL_NUMBERING") = "1"
				Session.Property("FEATURE_TRAILER") = ""
				Session.Property("INSTALL_TRAILER") = ""
			case 4
				Session.Property("FEATURE_MACPAC") = ""
				Session.Property("INSTALL_MACPAC") = ""
				Session.Property("FEATURE_NUMBERING") = ""
				Session.Property("INSTALL_NUMBERING") = ""
				Session.Property("FEATURE_TRAILER") = "1"
				Session.Property("INSTALL_TRAILER") = "1"
			case 3
				Session.Property("FEATURE_MACPAC") = "1"
				Session.Property("INSTALL_MACPAC") = "1"
				Session.Property("FEATURE_NUMBERING") = "1"
				Session.Property("INSTALL_NUMBERING") = "1"
				Session.Property("FEATURE_TRAILER") = ""
				Session.Property("INSTALL_TRAILER") = ""
			case 5
				Session.Property("FEATURE_MACPAC") = "1"
				Session.Property("INSTALL_MACPAC") = "1"
				Session.Property("FEATURE_NUMBERING") = ""
				Session.Property("INSTALL_NUMBERING") = ""
				Session.Property("FEATURE_TRAILER") = "1"
				Session.Property("INSTALL_TRAILER") = "1"
			case 6
				Session.Property("FEATURE_MACPAC") = ""
				Session.Property("INSTALL_MACPAC") = ""
				Session.Property("FEATURE_NUMBERING") = "1"
				Session.Property("INSTALL_NUMBERING") = "1"
				Session.Property("FEATURE_TRAILER") = "1"
				Session.Property("INSTALL_TRAILER") = "1"
			case 7
				Session.Property("FEATURE_MACPAC") = "1"
				Session.Property("INSTALL_MACPAC") = "1"
				Session.Property("FEATURE_NUMBERING") = "1"
				Session.Property("INSTALL_NUMBERING") = "1"
				Session.Property("FEATURE_TRAILER") = "1"
				Session.Property("INSTALL_TRAILER") = "1"
			case else
				bIsValidFeature = False
		end select
		
		'(Err.Number <> 0)
        If (bIsValidKey = False) OR (bIsValidFeature = False) Then
			Session.Property("LicenseKeyIsValid") = "0"
			ValidateSerial = 0
			Exit Function
		End If
		
        On error goto 0
		Session.Property("LicenseKeyIsValid") = "1"
		Session.Property("LICENSEKEY") = xSer
		Session.Property("Features") = Trim(xComponents)
		ValidateSerial = 0
    End if
End Function

Function DecryptComponent(StringToDecrypt)

    Dim dblCountLength
    Dim intLengthChar
    Dim strCurrentChar
    Dim dblCurrentChar
    Dim intCountChar
    Dim intRandomSeed
    Dim intBeforeMulti
    Dim intAfterMulti
    Dim intSubNinetyNine
    Dim intInverseAsc
    Dim xTemp
	
    For dblCountLength = 1 To Len(StringToDecrypt)
        
        intLengthChar = Mid(StringToDecrypt, dblCountLength, 1)
       
        strCurrentChar = Mid(StringToDecrypt, dblCountLength + 1, intLengthChar)
        dblCurrentChar = 0
        
        For intCountChar = 1 To Len(strCurrentChar)
			'Convert the variable 'strCurrent' from base 98 to base 10 and
			'place the value into the variable 'dblCurrentChar'
            dblCurrentChar = dblCurrentChar + (Asc(Mid(strCurrentChar, intCountChar, 1)) - 33) * (93 ^ (Len(strCurrentChar) - intCountChar))
        Next
        
        'Determine the random number that was used in the 'Encrypt' function
        intRandomSeed = Mid(dblCurrentChar, 3, 2)
        'Determine the number that represents the character without the random seed
        intBeforeMulti = Mid(dblCurrentChar, 1, 2) & Mid(dblCurrentChar, 5, 2)
        
        'Divide the number that represents the character by the random seed
        'and place that value into the variable 'intAfterMulti'
        intAfterMulti = intBeforeMulti / intRandomSeed
        'Subtract 99 from the variable 'intAfterMulti' and place that value
        'into the variable 'intSubNinetyNine'
        intSubNinetyNine = intAfterMulti - 99
        'Subtract the variable 'intSubNinetyNine' from 256 and place that
        'value into the variable 'intInverseAsc'
        intInverseAsc = 256 - intSubNinetyNine
        'Place the character equivalent of the variable 'intInverseAsc' at the
        'end of the function 'Decrypt'
        DecryptComponent = DecryptComponent & Chr(intInverseAsc)
        dblCountLength = dblCountLength + intLengthChar
    Next
	'DecryptComponent = xTemp
		
End Function


Function ValidDateKey(xDateKey)
    '   unencrypt Date
    
    '   remove garbage 1st digit
        xCode = Mid(xSer, 2)
    
    '    msgbox xCode
    
    '   store and remove random factor
    '    bRnd = Left(xCode, 1)
        xCode = Mid(xCode, 2)
        
    '   collect chars at even indexes
        For i = 2 To Len(xCode) Step 2
            xCodeMod = xCodeMod & Mid(xCode, i, 1)
        Next
        
        While Len(xCodeMod)
            x = Left(xCodeMod, 1)
            If Not IsNumeric(x) Then
                If IsReserved(x) Then
                    x = Asc(x) - 100
                Else
                    'non-reserved letter, so get mapping for lower case
                    x = MapLetter(LCase(x))
                End If
            End If
            
            xDate = xDate & x
            
    '       trim left char
            xCodeMod = Mid(xCodeMod, 2)
        Wend
    '    msgbox xDate
    '
        xPart1 = Mid(xDate, 3, 4)
        xPart2 = Mid(xDate, 7, 2)
        xPart3 = Mid(xDate, 1, 2)
        
    '    msgbox xPart1 & " " & xPart2 & " " & xPart3
        
        'UnEncryptDate = DateSerial(xPart1, xPart2, xPart3)
    
        'msgbox DateSerial(xPart1, xPart2, xPart3)
    
        xSerial = DateSerial(xPart1, xPart2, xPart3)
        
    '    msgbox xSerial
        
        d = xSerial
        
    '    msgbox "d= " & d
        
    '    msgbox d = #4/1/2099#
    '    msgbox d > Now
    '    msgbox CLng(d - Now) < 180
        
    '   valid key?
        ValidDateKey = (d = #4/1/2099#) Or _
                        (d > Now And (CLng(d - Now) < 180))
    

End Function



Function MapLetter(xLetter)

    Dim bDigit
    
    Select Case xLetter
    Case "q"
        bDigit = 9
    Case "r"
        bDigit = 8
    Case "s"
        bDigit = 7
    Case "t"
        bDigit = 6
    Case "u"
        bDigit = 5
    Case "v"
        bDigit = 4
    Case "w"
        bDigit = 3
    Case "x"
        bDigit = 2
    Case "y"
        bDigit = 1
    Case "z"
        bDigit = 0
    End Select
    
    MapLetter = bDigit

End Function

Function IsReserved(xLetter)
    Select Case xLetter
    Case "d", "e", "f", "g", "h", "i", "j", "k", "l", "m"
        IsReserved = True
    Case Else
        IsReserved = False
    End Select
End Function
